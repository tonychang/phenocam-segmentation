[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0) 

# Phenocam Segmentation with DeepLabV3+ 

This repository documents prototyping of phenocam segmentation using a Tensorflow implementation of DeepLabV3+ with CRF post processing to delineating multiple classes within a phenocam image. Image segmentation delineating class labels such as 'sky', 'bare ground', 'trees', 'grass', and etc. increases the utility of phenometric monitoring from terrestrial level cameras. Since plant species can vary in their leaf out cycle, discrimination of plant functional types within an image can allow pixel level green chromatic coordinate aggregation for increased accuracy. 

We utilize the DeepLabV3+ implementation by:
[https://github.com/Golbstein/Keras-segmentation-deeplab-v3.1](https://github.com/Golbstein/Keras-segmentation-deeplab-v3.1)

although at publication of this repository, there deeplab has become integrated into Tensorflow as a built in model. 

We initially apply tranfer learning to a trained DeepLabV3+ network using the [Freiburg Forest dataset](http://deepscene.cs.uni-freiburg.de/). This dataset has been successfully used in a variety of applications including: [off-road autonomous driving](https://www.ncbi.nlm.nih.gov/pubmed/31174299h) and [outdoor scene label classification](http://deepscene.cs.uni-freiburg.de/#demo). 

To collect phenocam images for model testing we additionally use the [phenocam api](https://github.com/katharynduffy/phenoSynth), a submodule of the phenoSynth R package developed by [Katharyn Duffy](https://github.com/katharynduffy). 

Additionally, to add model accuracy, we utilize the [phenocam vegetation segmentation](https://github.com/khufkens/phenocam_vegetation_segmentation) dataset of approximately 58K training images developed by [Koen Hufkens](https://github.com/khufkens). 

# Extracting PhenoCam data using phenocamapi R Package 

```{r}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

PhenoCam time-series are extracted time-series data obtained from ROI's for a given site. 
Each site has specific metadata including but not limited to how the site is set-up and where it is located, what vegetaion type is visible from the camera, and its climate regime. Each PhenoCam may have none to several ROI's per vegetation type. 

This R package is an effort to simplify data wrangling steps and finally merge them as a single dataframe.

# Installation
`phenocamapi` can be directly installed from the [GitHub repo](https://github.com/bnasr/phenocamapi):
```{r}
# installing the package from the GitHub repo
if(!require(devtools)) install.packages('devtools')
devtools::install_github('bnasr/phenocamapi')

# loading the package
library(phenocamapi)
```


# Usage
The vignette page contains useful information on how to use the R package.
```{r}
browseVignettes('phenocamapi')
```

Detailed guides on how to use each function has been provided in the manual page of each function.


