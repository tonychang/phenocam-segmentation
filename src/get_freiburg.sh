#!/bin/bash
mkdir -p data/OpenSourceDatasets
wget -nc http://deepscene.cs.uni-freiburg.de/static/datasets/freiburg_forest_multispectral_annotated.tar.gz -O data/freiburg_forest_multispectral_annotated.tar.gz && \ 
tar xvzf data/freiburg_forest_multispectral_annotated.tar.gz -C data/OpenSourceDatasets
