IMAGE=tonychangcsp/phenocamseg:latest 
#IMAGE=tonychangcsp/keras:latest
docker run -it --rm \
    --runtime=nvidia \
    --name jupyter \
    -v $(pwd):/content \
    -w /content \
    -p 8888:8888 \
    $IMAGE\
    jupyter notebook --port 8888 --ip 0.0.0.0 --no-browser --allow-root
