from PIL import Image
import numpy as np
import pandas as pd
from glob import glob
from scipy.io import loadmat
from skimage.color import rgb2gray

class DataLoader:
    def __init__(self, name, wd):
        self.wd = wd
        self.dsets = ['MSRC_ObjCategImageDatabase_v1',\
                      'MSRC_ObjCategImageDatabase_v2',\
                      'freiburg_forest_annotated', \
                      'msrcorid',\
                      'newsegmentations_mats',\
                      'HYTA']
        self.name = name
    def get_data(self):
        if self.name == 'msrcv1':
            return(self.get_msrcv1_data())
        if self.name == 'msrcv2':
            return(self.get_msrcv2_data())
        elif self.name == 'freiburg':
            return(self.get_freiburg_data())
        else:
            print('not a valid type!')
    def get_msrcv1_data(self):
        return(self.get_set_data(self.dsets[0],'GroundTruth', 'Images'))
    def get_msrcv2_data(self):
        return(self.get_set_data(self.dsets[1],'GroundTruth', 'Images'))
    def get_freiburg_data(self):
        return(self.get_set_data(self.dsets[2],'train/GT_color','train/rgb'))
    def get_image_list(self,dirc):
        ext = ['tif', 'png', 'jpg', 'bmp']
        imfiles = [glob('%s/*.%s'%(dirc,e)) for e in ext if glob('%s/*.%s'%(dirc,e))]
        flatten_list = lambda l: [item for sublist in l for item in sublist]
        imfiles = sorted(flatten_list(imfiles))
        return(imfiles)
    def get_images_from_list(self,im_file_list): 
        return([Image.open(i) for i in im_file_list])
    def get_set_data(self,dset_name,gt_ext,im_ext):
        im_wd = '%s/%s/%s'%(self.wd, dset_name, im_ext)
        gt_wd = '%s/%s/%s'%(self.wd, dset_name, gt_ext)
        im = self.get_images_from_list(self.get_image_list(im_wd))
        gt = self.get_images_from_list(self.get_image_list(gt_wd))
        return([im, gt]) 

class Data:
    #constants for directories
    def __init__(self, name, wd):
        loader = DataLoader(name, wd)
        self.im, self.gt = loader.get_data()
    def resize_data(self, nrows=None, ncols=None):
        '''
        if height or width None, resize uses the proportional scaling
        '''
        if not ncols:
            percent = (nrows/float(img.size[0]))
            ncols = int((float(img.size[1])*float(percent)))
        elif not nrows:
            percent = (ncols/float(img.size[1]))
            nrows = int((float(img.size[0])*float(percent)))
        for i in range(len(self.im)):
            self.im[i] = self.im[i].resize((nrows,ncols), Image.ANTIALIAS)
            self.gt[i] = self.gt[i].resize((nrows,ncols), Image.NEAREST)
    def one_hot_it(self):
        n = np.unique(np.array(self.gt))
        oht = []
        for i in range(len(self.gt)):
            oht.append(np.array([self.gt[i]==c for c in n]))
        self.gt = np.moveaxis(np.array(oht).astype('int16'),1,-1)
    def remap_data(self, mapping):
        fmap = {k:v for k,v in zip(mapping,np.arange(len(mapping)))}
        vfunc = np.vectorize(fmap.get)
        for i in range(len(self.gt)):
            remapped = vfunc(np.array(self.gt[i])[:,:,1])
            self.gt[i] = remapped.astype('int16')
