IMAGE=tonychangcsp/phenocamseg:1.13.2
#IMAGE=tonychangcsp/keras:latest
docker run -it --rm \
    --runtime=nvidia \
    --name jupyter \
    -v $(pwd):/content \
    -w /content \
    -v ~/phenocam-segmentation/data:/workspace/datasets \
    -p 8888:8888 \
    $IMAGE\
    jupyter notebook --port 8888 --ip 0.0.0.0 --no-browser --allow-root
